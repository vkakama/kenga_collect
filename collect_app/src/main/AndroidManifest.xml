<?xml version="1.0" encoding="utf-8"?>
<!--
     Copyright (C) 2009 University of Washington Licensed under the Apache
	License, Version 2.0 (the "License"); you may not use this file except in
	compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
	Unless required by applicable law or agreed to in writing, software distributed
	under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
	OR CONDITIONS OF ANY KIND, either express or implied. See the License for
	the specific language governing permissions and limitations under the License.
-->
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="org.odk.collect.android"
    android:versionCode="1058"
    android:versionName="1.4.8" >

    <uses-sdk
        android:minSdkVersion="8"
        android:targetSdkVersion="8" />

    <uses-feature
        android:name="android.hardware.location"
        android:required="false" />
    <uses-feature
        android:name="android.hardware.location.network"
        android:required="false" />
    <uses-feature
        android:name="android.hardware.location.gps"
        android:required="false" />
    <uses-feature
        android:name="android.hardware.telephony"
        android:required="false" />
    <uses-feature
        android:name="android.hardware.wifi"
        android:required="false" />

    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
    <uses-permission android:name="android.permission.USE_CREDENTIALS" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <!-- added by vkakama@gmail.com -->
    <uses-permission android:name="android.permission.AUTHENTICATE_ACCOUNTS" />
    <uses-permission android:name="android.permission.MANAGE_ACCOUNTS" />
    <uses-permission android:name="android.permission.READ_SYNC_SETTINGS" />
    <uses-permission android:name="android.permission.WRITE_SYNC_SETTINGS" />

    <!--
          for Maps v2 functionality, want:
    	uses-feature android:glEsVersion="0x00020000" android:required="false"
    	BUT, the gl setting is not modified by the required parameter, so
    	do not declare anything here - detect capabilities at runtime.
    -->

    <permission
        android:name="org.odk.collect.android.permission.MAPS_RECEIVE"
        android:protectionLevel="signature" />

    <uses-permission android:name="org.odk.collect.android.permission.MAPS_RECEIVE" />
    <uses-permission android:name="com.google.android.providers.gsf.permission.READ_GSERVICES" />

    <supports-screens
        android:anyDensity="true"
        android:largeScreens="true"
        android:normalScreens="true"
        android:resizeable="true"
        android:smallScreens="true"
        android:xlargeScreens="true" />

    <application
        android:name=".application.Collect"
        android:icon="@drawable/kenga_logo"
        android:installLocation="auto"
        android:label="@string/app_name"
        android:largeHeap="true"
        android:supportsRtl="true"
        android:theme="@style/Collect" >
        <provider
            android:name=".provider.FormsProvider"
            android:authorities="org.odk.collect.android.provider.odk.forms"
            android:exported="true" />
        <provider
            android:name=".provider.InstanceProvider"
            android:authorities="org.odk.collect.android.provider.odk.instances"
            android:exported="true" />

        <!-- =========================Sync Adapter settings ============================ -->


        <!-- Declare the my content provider stub for the sync adapter -->
        <provider
            android:name=".provider.ExtContentProvider"
            android:authorities="org.odk.collect.android.provider.odk.entities"
            android:exported="true"
            android:syncable="true" />
        <!--
            This service implements our SyncAdapter. It needs to be exported, so that the system
            sync framework can access it.
            android:process=":sync" removed from service tag
        -->
        <service
            android:name=".sync.OdxSyncService"
            android:exported="true" >
            <intent-filter>
                <action android:name="android.content.SyncAdapter" />
            </intent-filter>

            <meta-data
                android:name="android.content.SyncAdapter"
                android:resource="@xml/syncadapter" />
        </service>

        <!-- Declare the Authenticator in the Manifest -->
        <service android:name=".sync.authenticator.AuthenticatorService" >
            <intent-filter>
                <action android:name="android.accounts.AccountAuthenticator" />
            </intent-filter>

            <meta-data
                android:name="android.accounts.AccountAuthenticator"
                android:resource="@xml/authenticator" />
        </service>

        <!-- =========================END============================ -->

        <activity
            android:name=".activities.MainMenuActivity"
            android:configChanges="orientation"
            android:label="@string/app_name" >
        </activity>
        <activity
            android:name=".activities.FormEntryActivity"
            android:configChanges="orientation"
            android:label="@string/app_name"
            android:windowSoftInputMode="adjustResize" >
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <action android:name="android.intent.action.EDIT" />

                <category android:name="android.intent.category.DEFAULT" />

                <data android:mimeType="vnd.android.cursor.item/vnd.odk.form" />
                <data android:mimeType="vnd.android.cursor.item/vnd.odk.instance" />
            </intent-filter>
        </activity>
        <activity
            android:name=".activities.NotificationActivity"
            android:excludeFromRecents="true"
            android:label="@string/app_name"
            android:launchMode="singleTask"
            android:taskAffinity="" />
        <activity
            android:name=".activities.DrawActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.GoogleDriveActivity"
            android:label="@string/app_name"
            android:windowSoftInputMode="stateHidden" />
        <activity
            android:name=".activities.InstanceChooserList"
            android:label="@string/app_name" />

        <intent-filter>
            <action android:name="android.intent.action.VIEW" />
            <action android:name="android.intent.action.EDIT" />

            <category android:name="android.intent.category.DEFAULT" />

            <data android:mimeType="vnd.android.cursor.dir/vnd.odk.instance" />
        </intent-filter>

        <activity
            android:name=".activities.InstanceChooserTabs"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.FormChooserList"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <action android:name="android.intent.action.EDIT" />
                <action android:name="android.intent.action.PICK" />

                <category android:name="android.intent.category.DEFAULT" />

                <data android:mimeType="vnd.android.cursor.dir/vnd.odk.form" />
            </intent-filter>
        </activity>
        <activity
            android:name=".activities.FormManagerList"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.FormDownloadList"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.DataManagerList"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.FileManagerTabs"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.GoogleSheetsUploaderActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.InstanceUploaderList"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <action android:name="android.intent.action.EDIT" />

                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>
        <activity
            android:name=".activities.InstanceUploaderActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".preferences.PreferencesActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".preferences.GooglePreferencesActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".preferences.OtherPreferencesActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".preferences.AggregatePreferencesActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".preferences.AdminPreferencesActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.FormHierarchyActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.GeoPointActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.GeoPointMapActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.GeoPointMapNotDraggableActivity"
            android:label="@string/app_name" />
        <activity
            android:name=".activities.BearingActivity"
            android:label="@string/app_name" />
        <activity
                android:name=".activities.entities.EntityListActivity"
                android:label="@string/app_name" />
        <activity
                android:name=".activities.entities.EntityDataActivity"
                android:label="@string/app_name"
                android:windowSoftInputMode="stateHidden"/>
        <activity
                android:name=".activities.entities.SyncFilterDownloadList"
                android:label="@string/app_name"
                android:windowSoftInputMode="stateHidden"
                android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize"/>
        <activity
                android:name=".activities.entities.PrefillActivity"
                android:label="@string/app_name"
                android:noHistory="true"
                android:windowSoftInputMode="stateHidden">
            <intent-filter>
                <action android:name="org.odk.collect.android.activities.entities.PrefillActivity"/>
                <category android:name="android.intent.category.DEFAULT"/>
            </intent-filter>
        </activity>
        <activity
            android:name=".activities.SplashScreenActivity"
            android:label="@string/app_name"
            android:theme="@android:style/Theme.Dialog" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        <!-- Enable Shortcuts for Command Actions -->
        <activity
            android:name=".activities.AndroidShortcuts"
            android:label="ODK Form"
            android:theme="@android:style/Theme.Translucent.NoTitleBar" >
            <intent-filter>
                <action android:name="android.intent.action.CREATE_SHORTCUT" />

                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>

        <receiver
            android:name=".receivers.NetworkReceiver"
            android:enabled="true" >
            <intent-filter>
                <action android:name="android.net.conn.CONNECTIVITY_CHANGE" />
            </intent-filter>
            <intent-filter>
                <action android:name="org.odk.collect.android.FormSaved" />
            </intent-filter>
        </receiver>

        <meta-data
            android:name="com.google.android.geo.API_KEY"
            android:value="AIzaSyBS-JQ-dnaZ_8qsbvSyr_I3rTPFd5fJsYI" />
        <meta-data
            android:name="com.google.android.gms.version"
            android:value="@integer/google_play_services_version" />  <!-- integer/google_play_services_version -->

        <uses-library
            android:name="com.google.android.maps"
            android:required="false" />

    </application>

</manifest>
