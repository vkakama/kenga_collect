package org.odk.collect.android.external.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import org.odk.collect.android.application.Collect;
import org.odk.collect.android.external.model.EntityData;
import org.odk.collect.android.external.model.TableAttr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 20-Jan-16.
 */
public class EntityDataDao {
    public static final String TAG = "EntityDataDao";

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper() {
            super(Collect.getInstance(), DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    private static final String DATABASE_NAME = "external_entities.db";
    private static final String DATABASE_TABLE = "entities";
    private static final int DATABASE_VERSION = 1;
    private Context context;

    public EntityDataDao(Context context) {
        this.context = context;
    }


    public void createTable(String tableName, List<TableAttr> tableAttrs) {
        SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
        String attrQuery = "";
        for (TableAttr attr : tableAttrs) {
            if (attrQuery.equals("")) {
                attrQuery += " '" + attr.getColumn() + "' " + attr.getDatatype() + (attr.isPrimaryKey() ? " PRIMARY KEY NOT NULL UNIQUE " : "");
            } else {
                attrQuery += ", '" + attr.getColumn() + "' " + attr.getDatatype() + (attr.isPrimaryKey() ? " PRIMARY KEY NOT NULL UNIQUE " : "");
            }
        }

        String query = "CREATE TABLE IF NOT EXISTS  '" + tableName + "' (" + attrQuery + ");";
        Log.e(TAG, "Query:" + query);
        db.execSQL(query);
        db.close();
    }

    public void deleteTable(String tableName){
        try{
            SQLiteDatabase db =  new DatabaseHelper().getWritableDatabase();
            db.delete(tableName,null,null);
            db.close();
        }catch (Exception ex){
            Log.e("Error: ", ex.getMessage());
        }
    }

    public void insertIntoEntityDatatable(String tableName, EntityData entityData) {
        try {
            SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
            String insertQuery = "INSERT INTO " + tableName + " VALUES ('" + entityData.getKeyField() + "','" + entityData.getDisplayField() + "')";
            Log.e(TAG, insertQuery);
            db.execSQL(insertQuery);
            db.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void insertEntityDataInTX(String tableName, List<EntityData> entityDataList) {
        try {
            String sql = "INSERT INTO " + tableName + " VALUES (?,?)";
            SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
            db.beginTransactionNonExclusive();
            SQLiteStatement statement = db.compileStatement(sql);
            for (EntityData entityData : entityDataList) {
                try{
                    statement.bindString(1, entityData.getKeyField());
                    statement.bindString(2, entityData.getDisplayField());
                    statement.execute();
                    statement.clearBindings();
                }catch (Exception ex){
                    Log.e(TAG,ex.getMessage());
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<EntityData> loadAll(String tableName, String keyField, String displayField) {
        SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
        List<EntityData> entityDataList = new ArrayList<EntityData>();
        String query = "SELECT * FROM " + tableName;
        Log.e(TAG, query);
        Cursor cursor = db.query(tableName, new String[]{keyField, displayField}, null, null, null, null, displayField + " ASC");
        if (cursor.moveToFirst()) {
            Log.e(TAG, cursor.getCount() + " Record found");
            do {
                EntityData entityData = new EntityData();
                entityData.setKeyField(cursor.getString(cursor.getColumnIndexOrThrow(keyField)));
                entityData.setDisplayField(cursor.getString(cursor.getColumnIndexOrThrow(displayField)));
                entityDataList.add(entityData);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        db.close();
        return entityDataList;

    }

    public List<EntityData> loadAll(String tableName, String keyField, List<String> displayFields) {
        SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
        List<String> fields = displayFields;
        fields.set(0, keyField);
        List<EntityData> entityDataList = new ArrayList<EntityData>();
        String query = "SELECT * FROM " + tableName;
        Log.e(TAG, query);
        Cursor cursor = db.query(tableName, fields.toArray(new String[fields.size()]), null, null, null, null, displayFields.get(0) + " ASC");
        if (cursor.moveToFirst()) {
            Log.e(TAG, cursor.getCount() + " Record found");
            do {
                EntityData entityData = new EntityData();
                entityData.setKeyField(cursor.getString(cursor.getColumnIndexOrThrow(keyField)));
                String displayFld = "";
                for (String field : fields) {
                    String attr = cursor.getString(cursor.getColumnIndexOrThrow(field));
                    if (attr != null) {
                        displayFld += attr + ",";
                    }
                }
                entityData.setDisplayField(displayFld);
                entityDataList.add(entityData);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return entityDataList;

    }

    public Cursor loadAllIntoCursor(String tableName, String keyField, String displayField) {
        SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
        String query = "SELECT * FROM " + tableName;
        Log.e(TAG, query);
        Cursor cursor = db.query(tableName, new String[]{keyField, displayField}, null, null, null, null, displayField + " ASC");
        return cursor;

    }

    public boolean deleteAllData(String tableName) {
        SQLiteDatabase db = new DatabaseHelper().getWritableDatabase();
        int deleted = 0;
        deleted = db.delete(tableName, null, null);
        Log.e(TAG, Integer.toString(deleted));
        db.close();
        return deleted > 0;
    }

}
