package org.odk.collect.android.external.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by victor on 01-Oct-15.
 */
public class EntityData implements Parcelable {
    String keyField;
    String displayField;

    public EntityData() {
    }

    public EntityData(String keyField, String displayField) {
        this.keyField = keyField;
        this.displayField = displayField;
    }

    public String getKeyField() {
        return keyField;
    }

    public void setKeyField(String keyField) {
        this.keyField = keyField;
    }

    public String getDisplayField() {
        return displayField;
    }

    public void setDisplayField(String displayField) {
        this.displayField = displayField;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(keyField);
        parcel.writeString(displayField);
    }

    public static final Parcelable.Creator<EntityData> CREATOR = new Creator<EntityData>() {
        @Override
        public EntityData createFromParcel(Parcel parcel) {
            EntityData entityData = new EntityData();
            entityData.keyField = parcel.readString();
            entityData.displayField = parcel.readString();
            return entityData;
        }

        @Override
        public EntityData[] newArray(int size) {
            return new EntityData[size];
        }
    };

    public static EntityData fromCursor(Cursor cursor, String keyField, String displayField) {
        EntityData entityData = new EntityData();
        entityData.setKeyField(cursor.getString(cursor.getColumnIndexOrThrow(keyField)));
        entityData.setDisplayField(cursor.getString(cursor.getColumnIndexOrThrow(displayField)));
        return  entityData;
    }
}
