package org.odk.collect.android.sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import org.odk.collect.android.events.BusProvider;
import org.odk.collect.android.events.DbChangeEvent;
import org.odk.collect.android.events.SyncEvent;
import org.odk.collect.android.external.dao.EntityDataDao;
import org.odk.collect.android.external.dao.ExEntityDao;
import org.odk.collect.android.external.model.EntityData;
import org.odk.collect.android.external.model.ExEntity;
import org.odk.collect.android.external.model.TableAttr;
import org.odk.collect.android.preferences.PreferencesActivity;
import org.odk.collect.android.restservices.EntityRestClient;
import org.odk.collect.android.restservices.ServiceGenerator;
import org.odk.collect.android.utilities.ExEntityUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by victor on 23-Oct-15.
 */
public class ManualSync {
    public static final String TAG = "ManualSync";
    public static String endPointUrl = ServiceGenerator.API_BASE_URL;
    private SQLiteDatabase db;

    private SharedPreferences sharedPreferences;
    private EntityRestClient restClient;

    private Context context;
    private EntityDataDao entityDataDao;
    private ExEntityDao entityDao;

    public ManualSync(Context context) {
        this.context = context;
        init(context);
    }

    private void init(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            endPointUrl = sharedPreferences.getString("url", ServiceGenerator.API_BASE_URL);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        restClient = getEntityRestClient();
        entityDataDao = new EntityDataDao(context);
        entityDao = new ExEntityDao();
        setMisUrl();
    }

    public void performManualSync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_START));
                    syncPreloadEntities();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_END));
                }
            }
        }).start();

    }

    public void performDataSync(final ExEntity exEntity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_START));
                    syncDataForEnttity(exEntity);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_END));
                }
            }
        }).start();
    }


    private boolean createDataTableForEntity(ExEntity exEntity) {
        try {
            String displayField = exEntity.getDisplayField().split(EntitySyncAdapter.DISPLAY_FIELD_SEPERATORE)[0];
            List<TableAttr> tableAttrs = new ArrayList<TableAttr>();
            tableAttrs.add(new TableAttr(exEntity.getKeyField(), true, "TEXT"));
            tableAttrs.add(new TableAttr(displayField, false, "TEXT"));

            EntityDataDao myDbHelper = new EntityDataDao(context);
            myDbHelper.createTable(exEntity.getTableName(), tableAttrs);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public void syncPreloadEntities() {
        try {
            Call<List<ExEntity>> call = restClient.getPreloadEntities(getPath());
            call.enqueue(new Callback<List<ExEntity>>() {
                @Override
                public void onResponse(Call<List<ExEntity>> call, Response<List<ExEntity>> response) {
                    List<ExEntity> odxEntityList = response.body();
                    if (odxEntityList != null) {
                        Log.e(TAG, "new preload entities downloaded:" + odxEntityList.size());
                        insertEntities(odxEntityList);
                        syncData();
                    }
                }

                @Override
                public void onFailure(Call<List<ExEntity>> call, Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void insertEntities(List<ExEntity> odxEntityList) {
        if (odxEntityList.isEmpty()) {
            return;
        }
        entityDao.deleteAll();
        for (ExEntity entity : odxEntityList) {
            insertEntity(entity);
        }
    }

    public void insertEntity(ExEntity exEntity){
        entityDao.deleteExEntity(exEntity);
        Log.e(TAG, "new preload entity found:" + exEntity.getTableName());
//            delete existing entity Data table
        entityDataDao.deleteTable(exEntity.getTableName());
        exEntity.setId(UUID.randomUUID().toString());
        entityDao.insert(exEntity);
        //Create data table
        createDataTableForEntity(exEntity);
    }

    public void syncData() {
        List<ExEntity> odxEntityList = entityDao.loadAll();
        Log.e(TAG, "Entity to fetch data for:" + odxEntityList.size());
        for (final ExEntity odxEntity : odxEntityList) {
            Log.e(TAG, "Entity to fetch data for:" + odxEntity.getTableName());
            syncDataForEnttity(odxEntity);
        }
    }

    private void syncDataForEnttity(final ExEntity entity) {
        Call<List<EntityData>> call = restClient.getEntityData(getPath(), entity.getTableName(), entity.getKeyField(), entity.getDisplayField());
        call.enqueue(new Callback<List<EntityData>>() {
            @Override
            public void onResponse(Call<List<EntityData>> call, Response<List<EntityData>> response) {
                List<EntityData> entityDataList = response.body();
                try {
                    if (entityDataList != null) {
                        insertEntityData(entityDataList, entity);
                        if (!entityDataList.isEmpty()) {
                            BusProvider.getInstance().post(new DbChangeEvent(entityDataList, entity));
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_END));
                }
            }

            @Override
            public void onFailure(Call<List<EntityData>> call, Throwable t) {
                t.printStackTrace();
                BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_END));
            }
        });
    }

    public void insertEntityData(List<EntityData> entityDataList, ExEntity entity) {
        Log.e(TAG, "Deleting existing data..");
        entityDataDao.deleteAllData(entity.getTableName());
        Log.e(TAG, "Data deleted,Inserting new data...");

        for (EntityData entityData : entityDataList) {
            Log.e(TAG, "downloaded data from server:[Key:" + entityData.getKeyField() + ",DisplayField:" + entityData.getDisplayField());
            entityDataDao.insertIntoEntityDatatable(entity.getTableName(), entityData);
        }

        Log.e(TAG, "Data Successfully inserted for this table " + entity.getTableName());
    }

    public void insertEntityDataTx(List<EntityData> entityDataList, ExEntity entity) {
        Log.e(TAG, "Deleting existing data..");
        entityDataDao.deleteAllData(entity.getTableName());
        Log.e(TAG, "Data deleted,Inserting new data...");
        entityDataDao.insertEntityDataInTX(entity.getTableName(), entityDataList);
        Log.e(TAG, "Data Successfully inserted for this table " + entity.getTableName());
    }

    public EntityRestClient getEntityRestClient() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String username = sharedPreferences.getString(PreferencesActivity.KEY_USERNAME, null);
        String password = sharedPreferences.getString(PreferencesActivity.KEY_PASSWORD, null);
        return ServiceGenerator.createService(EntityRestClient.class , username, password);
    }

    public boolean platformSettingsEntered() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String username = sharedPreferences.getString(PreferencesActivity.KEY_USERNAME, null);
        String password = sharedPreferences.getString(PreferencesActivity.KEY_PASSWORD, null);
        if (username == null || password == null) {
            return false;
        }
        return true;
    }


    public void setMisUrl() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultMisUrl = sharedPreferences.getString(PreferencesActivity.KEY_MIS_URL,null);
        if(defaultMisUrl!=null&&!defaultMisUrl.equals("")&&ExEntityUtils.isValidUrl(defaultMisUrl)){
            if(!defaultMisUrl.endsWith("/")){
                defaultMisUrl+="/";
            }
            ServiceGenerator.API_BASE_URL = defaultMisUrl;
            return;
        }
        String kengaUrl =  sharedPreferences.getString(PreferencesActivity.KEY_SERVER_URL,null);
        if(kengaUrl!=null){
            String misUrl = ExEntityUtils.generateMisUrlFromKenga(kengaUrl);
            ServiceGenerator.API_BASE_URL = misUrl;
        }
    }

    public void setMisUrl(String url){
        ServiceGenerator.API_BASE_URL = url;
    }

    public String getPath(){
        String path = ServiceGenerator.PATH;
        String misUrl = ServiceGenerator.API_BASE_URL;
        if(misUrl!=null){
            Uri uri = Uri.parse(misUrl);
            path = uri.getPath().replaceAll("/","");
        }
        return path;
    }

    public List<ExEntity> loadEntitiesFromDb() {
        List<ExEntity> exEntities = entityDao.loadAll();
        return exEntities;
    }

    public ExEntity findEntityByTableName(String tableName){
        return entityDao.getExEntity("TABLE_NAME",tableName);
    }

}
