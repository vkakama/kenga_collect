package org.odk.collect.android.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by victor on 30-Sep-15.
 */
public class OdxSyncService extends Service {
    private static final String TAG = "OdxRestService";

    private static final Object adapterLock = new Object();
    private static EntitySyncAdapter entitySyncAdapter = null;

    @Override
    public void onCreate(){
        super.onCreate();
        Log.e(TAG,"onCreate");
        synchronized (adapterLock){
            if(entitySyncAdapter == null){
                entitySyncAdapter = new EntitySyncAdapter(getApplicationContext(),true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent){
        return entitySyncAdapter.getSyncAdapterBinder();
    }
}
