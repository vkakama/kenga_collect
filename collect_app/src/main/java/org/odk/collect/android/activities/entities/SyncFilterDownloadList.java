package org.odk.collect.android.activities.entities;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.squareup.otto.Subscribe;
import org.odk.collect.android.R;
import org.odk.collect.android.application.Collect;
import org.odk.collect.android.events.BusProvider;
import org.odk.collect.android.events.SyncEvent;
import org.odk.collect.android.external.model.EntityData;
import org.odk.collect.android.external.model.ExEntity;
import org.odk.collect.android.external.model.PrefillFilter;
import org.odk.collect.android.preferences.PreferencesActivity;
import org.odk.collect.android.sync.ManualSync;
import org.odk.collect.android.utilities.ExEntityUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kakavi on 5/4/2016.
 */
public class SyncFilterDownloadList extends ListActivity {
    private static final String TAG = "SyncFilterDownloadList";
    private ProgressDialog progressDialog;
    private Button syncButton;
    private Button downloadDataButton;
    private Button getSelectedButton;
    private Button toggleButton;
    private String mAlertMsg;
    private ManualSync restClient;
    private List<PrefillFilter> prefillFilters = new ArrayList<>();
    private ArrayAdapter filterListAdapter;
    private PrefillFilter currentFilter;
    private boolean toggleAll = false;
    //    this listview can either be showing entities or filters.
    private boolean isInfilterView = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restClient = new ManualSync(this);
        setContentView(R.layout.remote_file_manage_list);
        mAlertMsg = getString(R.string.please_wait);
        createProgressDialogue(mAlertMsg);
        getSelectedButton = (Button) findViewById(R.id.add_button);
        getSelectedButton.setText(R.string.more_filters);
        getSelectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSyncEvent();
                if (isInfilterView) {
                    getSelectedFilters();
                    downloadFilters(prefillFilters);
                } else {
//                    after selecting entity
                    ExEntity selectedEntity = (ExEntity) getListView().getItemAtPosition(getListView().getCheckedItemPosition());
                    restClient.insertEntity(selectedEntity);
                    prefillFilters.add(new PrefillFilter("", "", 0, null, selectedEntity.getTableName()));
                    downloadFilters(prefillFilters);
                    isInfilterView = true;
                    getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                }
            }
        });

        downloadDataButton = (Button) findViewById(R.id.refresh_button);
        downloadDataButton.setText("Download Data");
        downloadDataButton.setEnabled(false);
        downloadDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSelectedFilters();
                downloadPrefillData();
            }
        });

        toggleButton = (Button) findViewById(R.id.toggle_button);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSwitch();
                for (int i = 0; i < getListView().getCount(); i++) {
                    if (toggleAll) {
                        getListView().setItemChecked(i, true);
                    } else {
                        getListView().setItemChecked(i, false);
                    }
                }
            }
        });

        // need white background before load
        getListView().setBackgroundColor(Color.WHITE);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        getListView().setItemsCanFocus(false);
    }

    private void downloadPrefillData() {
        startSyncEvent();
        if (prefillFilters.isEmpty()) {
            stopSyncEvent();
            ExEntityUtils.alertStayOnCurrent(SyncFilterDownloadList.this, "Info", "No Filters downloaded,Please try again");
        }
        final ExEntity exEntity = restClient.findEntityByTableName(prefillFilters.get(0).getTableName());

        if(exEntity == null){
            stopSyncEvent();
            ExEntityUtils.alertStayOnCurrent(SyncFilterDownloadList.this, "Info", "Unable to download data Please try again");
        }
        exEntity.setPrefillFilterList(prefillFilters);
        Call<List<EntityData>> call = restClient.getEntityRestClient().downloadPrefillData(restClient.getPath(), exEntity);
        call.enqueue(new Callback<List<EntityData>>() {
            @Override
            public void onResponse(Call<List<EntityData>> call, Response<List<EntityData>> response) {
                try {
                    List<EntityData> entityDataList = response.body();
                    if (entityDataList != null) {
                        restClient.insertEntityDataTx(entityDataList, exEntity);
                    }
                    stopSyncEvent();
                    ExEntityUtils.alert(SyncFilterDownloadList.this, "Dwonload Result", "Prefill Data Successfully Downloaded");
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<EntityData>> call, Throwable t) {
                stopSyncEvent();
                if (t instanceof IOException) {
                    ExEntityUtils.alertStayOnCurrent(SyncFilterDownloadList.this, "Network Failure", "Lost Connection Please try again");
                }

            }
        });
    }

    private void getSelectedFilters() {
        SparseBooleanArray checked = getListView().getCheckedItemPositions();
        ArrayList<String> selectedItems = new ArrayList<>();
        for (int i = 0; i < checked.size(); i++) {
            int position = checked.keyAt(i);
            if (checked.valueAt(i)) {
                selectedItems.add(filterListAdapter.getItem(position).toString());
            }
        }
        if (!selectedItems.isEmpty()) {
            String values = TextUtils.join(",", selectedItems);
            currentFilter.setValue(values);
            prefillFilters.add(currentFilter);
        } else {
            Toast.makeText(this, "Please Select atleast one filter", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Collect.getInstance().getActivityLogger().logOnStart(this);
    }

    @Override
    protected void onStop() {
        Collect.getInstance().getActivityLogger().logOnStop(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    protected void onResume() {
        super.onResume();
//      register for db change event,sync event
        try {
            BusProvider.getInstance().register(this);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (restClient.platformSettingsEntered()) {
                        startSyncEvent();
                        downloadPreFillEntities();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                ExEntityUtils.alert(SyncFilterDownloadList.this,"Authentication needed","Please set your username and password under platform settings");
                                authPromptDialog().show();
                            }
                        });
                    }
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private ProgressDialog createProgressDialogue(String alertMessage) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.downloading_data));
        progressDialog.setMessage(alertMessage);
        progressDialog.setIcon(android.R.drawable.ic_dialog_info);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return progressDialog;
    }

    private void downloadFilters(final List<PrefillFilter> prefillFilters) {
        Call<PrefillFilter> call = restClient.getEntityRestClient().getPrefillFilters(restClient.getPath(), prefillFilters);
        call.enqueue(new Callback<PrefillFilter>() {
            @Override
            public void onResponse(Call<PrefillFilter> call, Response<PrefillFilter> response) {
                PrefillFilter prefillFilter = response.body();
                if (prefillFilter != null) {
                    currentFilter = prefillFilter;
                    filterListAdapter = new ArrayAdapter(SyncFilterDownloadList.this, android.R.layout.simple_list_item_multiple_choice, currentFilter.getDataList());
                    setListAdapter(filterListAdapter);
                    ((TextView) findViewById(R.id.remote_file_label)).setText(currentFilter.getField());
                    if ((prefillFilters.size()) == currentFilter.getFilterNumber()) {
                        getSelectedButton.setEnabled(false);
                        downloadDataButton.setEnabled(true);
                        getSelectedButton.setText(R.string.more_filters);
                    }
                } else {
                    Toast.makeText(SyncFilterDownloadList.this, "There are no Filters to download", Toast.LENGTH_LONG).show();
                }
                stopSyncEvent();
            }

            @Override
            public void onFailure(Call<PrefillFilter> call, Throwable t) {
                Log.e(TAG, t.getMessage() + "");
                t.printStackTrace();
                stopSyncEvent();
                if (t instanceof IOException) {
                    ExEntityUtils.alertStayOnCurrent(SyncFilterDownloadList.this, "Network Failure", "Lost Connection Please try again");
                }
            }
        });
    }

    private void downloadPreFillEntities() {
        try {
            Call<List<ExEntity>> call = restClient.getEntityRestClient().getPreloadEntities(restClient.getPath());
            call.enqueue(new Callback<List<ExEntity>>() {
                @Override
                public void onResponse(Call<List<ExEntity>> call, Response<List<ExEntity>> response) {
                    Log.e(TAG, "Response Code:" + response.code());
                    if (response.code() == 401 || response.code() == 404) {
                        authPromptDialog().show();
                    } else {
                        List<ExEntity> odxEntityList = response.body();
                        if (odxEntityList != null) {
                            Log.e(TAG, "new preload entities downloaded:" + odxEntityList.size());
                            filterListAdapter = new ArrayAdapter(SyncFilterDownloadList.this, android.R.layout.simple_list_item_multiple_choice, odxEntityList);
                            setListAdapter(filterListAdapter);
                            ((TextView) findViewById(R.id.remote_file_label)).setText("Entities");
                            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                            getSelectedButton.setEnabled(true);
                            getSelectedButton.setText(R.string.get_filters);
                            downloadDataButton.setEnabled(false);
                        } else {
                            ExEntityUtils.alertStayOnCurrent(SyncFilterDownloadList.this, "Network Failure", "Lost Connection Please try again");
                        }
                    }
                    stopSyncEvent();
                }

                @Override
                public void onFailure(Call<List<ExEntity>> call, Throwable t) {
//                    show entities in db
                    if (t instanceof IOException) {
                        ExEntityUtils.alertStayOnCurrent(SyncFilterDownloadList.this, "Network Failure", "Lost Connection Please try again");
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Subscribe
    public void onReceiveSyncEvent(SyncEvent syncEvent) {
        Log.e(TAG, "Received Sync Trigger Event " + syncEvent.getStatus());

        if (syncEvent != null && syncEvent.getStatus().equals(SyncEvent.SYNC_START)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.show();
                }
            });
        } else if (syncEvent != null && syncEvent.getStatus().equals(SyncEvent.SYNC_END)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });
        }
    }

    private AlertDialog authPromptDialog() {
        Collect.getInstance().getActivityLogger().logAction(this, "onCreateDialog.AUTH_DIALOG", "show");
        AlertDialog.Builder b = new AlertDialog.Builder(this);

        LayoutInflater factory = LayoutInflater.from(this);
        final View dialogView = factory.inflate(R.layout.server_auth_dialog, null);

        // Get the server, username, and password from the settings
        final SharedPreferences settings =
                PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String server =
                settings.getString(PreferencesActivity.KEY_SERVER_URL,
                        getString(R.string.default_server_url));

        String formListUrl = getString(R.string.default_odk_formlist);
        final String url =
                server + settings.getString(PreferencesActivity.KEY_FORMLIST_URL, formListUrl);

        EditText username = (EditText) dialogView.findViewById(R.id.username_edit);
        String storedUsername = settings.getString(PreferencesActivity.KEY_USERNAME, null);
        username.setText(storedUsername);

        EditText password = (EditText) dialogView.findViewById(R.id.password_edit);
        String storedPassword = settings.getString(PreferencesActivity.KEY_PASSWORD, null);
        password.setText(storedPassword);

        b.setTitle(getString(R.string.server_requires_auth));
        b.setMessage(getString(R.string.server_auth_credentials, url));
        b.setView(dialogView);
        b.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Collect.getInstance().getActivityLogger().logAction(this, "onCreateDialog.AUTH_DIALOG", "OK");

                EditText username = (EditText) dialogView.findViewById(R.id.username_edit);
                EditText password = (EditText) dialogView.findViewById(R.id.password_edit);

                SharedPreferences.Editor editor = settings.edit();
                editor.putString(PreferencesActivity.KEY_USERNAME, username.getText().toString());
                editor.putString(PreferencesActivity.KEY_PASSWORD, password.getText().toString());
                editor.commit();
                downloadPreFillEntities();
            }
        });
        b.setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Collect.getInstance().getActivityLogger().logAction(this, "onCreateDialog.AUTH_DIALOG", "Cancel");
                        finish();
                    }
                });

        b.setCancelable(false);
        return b.create();
    }


    private void toggleSwitch() {
        if (toggleAll) {
            toggleAll = false;
        } else {
            toggleAll = true;
        }
    }

    private void startSyncEvent() {
        BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_START));
    }

    private void stopSyncEvent() {
        BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_END));
    }
}
