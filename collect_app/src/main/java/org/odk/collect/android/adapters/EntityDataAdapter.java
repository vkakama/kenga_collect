package org.odk.collect.android.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.odk.collect.android.R;
import org.odk.collect.android.external.model.EntityData;
import org.odk.collect.android.sync.EntitySyncAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 25-Jan-16.
 */
public class EntityDataAdapter extends ArrayAdapter<EntityData> implements Filterable {
    private static final String TAG = "EntityDataAdapter";
    private List<EntityData> entityDataList;
    private ArrayList<EntityData> filterList;
    private Activity parent;
    private ModelFilter filter;

    public EntityDataAdapter(Activity activity, List<EntityData> entityDatas){
        super(activity, R.layout.entity_data_activity,entityDatas);
        this.parent = activity;
        this.entityDataList = new ArrayList<>();
        entityDataList.addAll(entityDatas);
        filterList = new ArrayList<>();
        filterList.addAll(entityDataList);
        getFilter();
    }

    public Filter getFilter(){
        if(filter == null){
            filter = new ModelFilter();
        }
        return filter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        EntityData entityData = filterList.get(position);
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.three_item_multiple_choice,parent,false);

            viewHolder = new ViewHolder();
            TextView textView1 = (TextView)convertView.findViewById(R.id.text1);
            TextView textView2 = (TextView)convertView.findViewById(R.id.text2);
            TextView textView3 = (TextView)convertView.findViewById(R.id.text3);
            CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.checkbox);

            viewHolder.textView1 = textView1;
            viewHolder.textView2 = textView2;
            viewHolder.textView3 = textView3;
            viewHolder.checkBox = checkBox;
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        String[] displayField = entityData.getDisplayField().split(EntitySyncAdapter.DISPLAY_FIELD_SEPERATORE);
        if(displayField.length>0){
            viewHolder.textView1.setText(displayField[0].trim());
            viewHolder.textView2.setVisibility(View.INVISIBLE);
            viewHolder.textView3.setVisibility(View.INVISIBLE);
        }

        if(displayField.length>1){
            viewHolder.textView2.setText(displayField[1]);
            viewHolder.textView2.setVisibility(View.VISIBLE);
            viewHolder.textView3.setVisibility(View.INVISIBLE);
        }
        if(displayField.length>2){
            viewHolder.textView3.setText(displayField[2]);
            viewHolder.textView3.setVisibility(View.VISIBLE);
        }
        viewHolder.checkBox.setVisibility(View.INVISIBLE);

        return convertView;
    }


    static class ViewHolder{
        TextView textView1;
        TextView textView2;
        TextView textView3;
        CheckBox checkBox;
    }

    public void setEntityDataList(List<EntityData> entityDataList) {
        this.entityDataList.clear();
        this.entityDataList.addAll(entityDataList);
        notifyDataSetChanged();
    }
    private class ModelFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if(constraint !=null && constraint.toString().length()>0){
                ArrayList<EntityData> filteredItems = new ArrayList<EntityData>();
                for(int i=0,l=entityDataList.size();i<l;i++){
                    EntityData entityData = entityDataList.get(i);
                    if(entityData.getDisplayField().toLowerCase().contains(constraint)){
                        filteredItems.add(entityData);
                    }
                }
                results.count = filteredItems.size();
                results.values = filteredItems;
            }else{
                synchronized (this){
                    results.values = entityDataList;
                    results.count = entityDataList.size();
                }
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterList = (ArrayList<EntityData>)results.values;
            notifyDataSetChanged();
            clear();
            for(int i=0,l=filterList.size();i<l;i++){
                add(filterList.get(i));
                notifyDataSetChanged();
            }
        }
    }

}
