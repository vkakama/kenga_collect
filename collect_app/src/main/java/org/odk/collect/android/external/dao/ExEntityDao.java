package org.odk.collect.android.external.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import org.odk.collect.android.application.Collect;
import org.odk.collect.android.external.model.ExEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 20-Jan-16.
 */
public class ExEntityDao {
    public static final String TAG = "ExEntityDao";

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper() {
            super(Collect.getInstance(), DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createTable(db, true);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            dropTable(db, true);
            createTable(db, true);
        }
    }


    private static final String DATABASE_NAME = "external_entities.db";
    private static final String DATABASE_TABLE = "entities";
    private static final int DATABASE_VERSION = 1;
    private DatabaseHelper mDbHelper = null;
    private SQLiteDatabase mDb = null;
    private boolean mIsOpen = false;

    //    column Names
    private static final String KEY_ID = "ID";
    private static final String KEY_NAME = "NAME";
    private static final String KEY_TABLE_NAME = "TABLE_NAME";
    private static final String KEY_DISPLAY_FIELD = "DISPLAY_FIELD";
    private static final String KEY_KEY_FIELD = "KEY_FIELD";
    private static final String KEY_KEY_FILTER_FIELD = "KEY_FILTER_FIELD";
    private static final String KEY_KEY_FILTER_VALUES = "KEY_FILTER_VALUES";
    private static final String KEY_KEY_FILTER_JOINER = "KEY_FILTER_JOINER";

    public ExEntityDao() {
    }

    public boolean isOpen() {
        return mIsOpen;
    }

    public SQLiteDatabase getWritableDatabase() throws SQLException {
        SQLiteDatabase database = null;
        try {
            database = new DatabaseHelper().getWritableDatabase();
            createTable(database,true);
        } catch (SQLiteException e) {
            Log.e("Error: ", e.getMessage());
        }
        return database;
    }

    public SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase database = null;
        try {
            database = new DatabaseHelper().getReadableDatabase();
        } catch (SQLiteException e) {
            Log.e("Error: ", e.getMessage());
        }
        return database;
    }

    public void insert(ExEntity exEntity) {
        try {
            SQLiteDatabase database = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_ID, exEntity.getId());
            values.put(KEY_NAME, exEntity.getName());
            values.put(KEY_TABLE_NAME, exEntity.getTableName());
            values.put(KEY_DISPLAY_FIELD, exEntity.getDisplayField());
            values.put(KEY_KEY_FIELD, exEntity.getKeyField());
            values.put(KEY_KEY_FILTER_FIELD, exEntity.getFilterFld());
            values.put(KEY_KEY_FILTER_VALUES, exEntity.getFilterValues());
            values.put(KEY_KEY_FILTER_JOINER, exEntity.getFilterJoiner());
            database.insert(DATABASE_TABLE, null, values);
            database.close();
        } catch (Exception ex) {
            Log.e("Error: ", ex.getMessage());
        }
    }

    public ExEntity getExEntity(String field, String value) {
        ExEntity exEntity = null;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.query(DATABASE_TABLE, new String[]{KEY_ID, KEY_NAME, KEY_TABLE_NAME, KEY_DISPLAY_FIELD,
                            KEY_KEY_FIELD, KEY_KEY_FILTER_FIELD, KEY_KEY_FILTER_VALUES, KEY_KEY_FILTER_JOINER},
                    field + "= ?", new String[]{value}, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            exEntity = new ExEntity(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return exEntity;
    }

    public List<ExEntity> loadAll() {
        List<ExEntity> exEntities = new ArrayList<>();
        try {
            String selectQuery = "SELECT * FROM " + DATABASE_TABLE;
            SQLiteDatabase database = getReadableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ExEntity exEntity = new ExEntity();
                    exEntity.setId(cursor.getString(0));
                    exEntity.setName(cursor.getString(1));
                    exEntity.setTableName(cursor.getString(2));
                    exEntity.setDisplayField(cursor.getString(3));
                    exEntity.setKeyField(cursor.getString(4));
                    exEntity.setFilterFld(cursor.getString(5));
                    exEntity.setFilterValues(cursor.getString(6));
                    exEntity.setFilterJoiner(cursor.getString(7));

                    exEntities.add(exEntity);
                } while (cursor.moveToNext());
            }
            database.close();
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }
        return exEntities;
    }

    public int countExEntities() {
        String countQuery = "SELECT * FROM " + DATABASE_TABLE;
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int updateExEntity(ExEntity exEntity) {
        try {
            SQLiteDatabase database = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_ID, exEntity.getId());
            values.put(KEY_NAME, exEntity.getName());
            values.put(KEY_TABLE_NAME, exEntity.getTableName());
            values.put(KEY_DISPLAY_FIELD, exEntity.getDisplayField());
            values.put(KEY_KEY_FIELD, exEntity.getKeyField());
            values.put(KEY_KEY_FILTER_FIELD, exEntity.getFilterFld());
            values.put(KEY_KEY_FILTER_VALUES, exEntity.getFilterValues());
            values.put(KEY_KEY_FILTER_JOINER, exEntity.getFilterJoiner());

            return database.update(DATABASE_TABLE, values, KEY_TABLE_NAME + "= ?", new String[]{exEntity.getTableName()});
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }
        return -1;
    }

    public void deleteExEntity(ExEntity exEntity) {
        try {
            SQLiteDatabase database = getWritableDatabase();
            database.delete(DATABASE_TABLE, KEY_TABLE_NAME + " = ?", new String[]{exEntity.getTableName()});
            database.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            SQLiteDatabase database = getWritableDatabase();
            database.delete(DATABASE_TABLE, null, null);
            database.close();
        } catch (Exception ex) {
            Log.e("Error: ", ex.getMessage());
        }
    }

    /**
     * Creates the underlying database table.
     */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        try {
            String constraint = ifNotExists ? "IF NOT EXISTS " : "";
            db.execSQL("CREATE TABLE " + constraint + "'" + DATABASE_TABLE + "' (" + //
                    "'" + KEY_ID + "' TEXT PRIMARY KEY NOT NULL UNIQUE ," + // 0: id
                    "'" + KEY_NAME + "' TEXT NOT NULL UNIQUE ," + // 1: name
                    "'" + KEY_TABLE_NAME + "' TEXT NOT NULL UNIQUE ," + // 2: tableName
                    "'" + KEY_DISPLAY_FIELD + "' TEXT," + // 3: displayField
                    "'" + KEY_KEY_FIELD + "' TEXT," + // 4: keyField
                    "'" + KEY_KEY_FILTER_FIELD + "' TEXT," + // 5: filterField
                    "'" + KEY_KEY_FILTER_VALUES + "' TEXT," + // 6: filterValuesField
                    "'" + KEY_KEY_FILTER_JOINER + "' TEXT);"); //7: filterjoinerFld

        } catch (Exception ex) {
            Log.e("Error: ", ex.getMessage());
        }
    }

    /**
     * Drops the underlying database table.
     */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'" + DATABASE_TABLE + "'";
        db.execSQL(sql);
    }


}
