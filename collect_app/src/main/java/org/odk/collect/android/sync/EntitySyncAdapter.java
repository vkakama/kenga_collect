package org.odk.collect.android.sync;

import android.accounts.Account;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import org.odk.collect.android.application.Collect;
import org.odk.collect.android.events.BusProvider;
import org.odk.collect.android.events.DbChangeEvent;
import org.odk.collect.android.events.SyncEvent;
import org.odk.collect.android.external.dao.EntityDataDao;
import org.odk.collect.android.external.dao.ExEntityDao;
import org.odk.collect.android.external.model.EntityData;
import org.odk.collect.android.external.model.ExEntity;
import org.odk.collect.android.external.model.TableAttr;
import org.odk.collect.android.preferences.PreferencesActivity;
import org.odk.collect.android.restservices.EntityRestClient;
import org.odk.collect.android.restservices.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by victor on 30-Sep-15.
 */
public class EntitySyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = "EntitySyncAdapter";
    public static String DISPLAY_FIELD_SEPERATORE = ",";
    //  private static final String PATH = "c4c-mis";
    private SQLiteDatabase db;

    private SharedPreferences sharedPreferences;
    private ContentResolver contentResolver;
    private String endPointUrl = "";

    private Context context;
    private EntityDataDao entityDataDao;
    private ExEntityDao entityDao;
    private ManualSync restClient;

    public EntitySyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context = context;
        init(context);
    }

    public EntitySyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        this.context = context;
        init(context);
    }

    private void init(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            endPointUrl = sharedPreferences.getString("url", ManualSync.endPointUrl);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        contentResolver = context.getContentResolver();
        entityDataDao = new EntityDataDao(context);
        entityDataDao = new EntityDataDao(context);
        entityDao = new ExEntityDao();
        restClient = new ManualSync(context);
        restClient.setMisUrl();

    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_START));
                    restClient = new ManualSync(Collect.getInstance());
                    syncPreloadEntities();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();

    }

    private void syncPreloadEntities() {
        try {
            Call<List<ExEntity>> call = restClient.getEntityRestClient().getPreloadEntities(restClient.getPath());
            call.enqueue(new Callback<List<ExEntity>>() {
                @Override
                public void onResponse(Call<List<ExEntity>> call, Response<List<ExEntity>> response) { List<ExEntity> odxEntityList = response.body();
                    if (odxEntityList != null) {

                        Log.e(TAG, "new preload entities downloaded:" + odxEntityList.size());

                        entityDao.deleteAll();
                        for (ExEntity entity : odxEntityList) {
                            Log.e(TAG, "new preload entity found:" + entity.getTableName());
                            entity.setId(UUID.randomUUID().toString());
                            entityDao.insert(entity);
                            //Create data table
                            createDataTableForEntity(entity);

                        }
                        syncData();
                    }
                }

                @Override
                public void onFailure(Call<List<ExEntity>> call, Throwable t) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void syncData() {
        List<ExEntity> odxEntityList = entityDao.loadAll();
        Log.e(TAG, "Entity to fetch data for:" + odxEntityList.size());
        for (final ExEntity entity : odxEntityList) {
            Log.e(TAG, "Entity to fetch data for:" + entity.getTableName());

            Call<List<EntityData>> call = restClient.getEntityRestClient().getEntityData(restClient.getPath(), entity.getTableName(), entity.getKeyField(), entity.getDisplayField());
            call.enqueue(new Callback<List<EntityData>>() {
                @Override
                public void onResponse(Call<List<EntityData>> call, Response<List<EntityData>> response) { List<EntityData> entityDataList = response.body();
                    try {
                        if (entityDataList != null) {
                            Log.e(TAG, "Deleting existing data..");
                            entityDataDao.deleteAllData(entity.getTableName());
                            Log.e(TAG, "Data deleted,Inserting new data...");

                            for (EntityData entityData : entityDataList) {
                                Log.e(TAG, "downloaded data from server:[Key:" + entityData.getKeyField() + ",DisplayField:" + entityData.getDisplayField());
                                entityDataDao.insertIntoEntityDatatable(entity.getTableName(), entityData);
                            }

                            Log.e(TAG, "Data Successfully inserted for this table " + entity.getTableName());
                            if (!entityDataList.isEmpty()) {
                                BusProvider.getInstance().post(new DbChangeEvent(entityDataList, entity));
                            }
                            BusProvider.getInstance().post(new SyncEvent(SyncEvent.SYNC_END));
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<EntityData>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    private boolean createDataTableForEntity(ExEntity entity) {
        try {
            String displayField = entity.getDisplayField().split(DISPLAY_FIELD_SEPERATORE)[0];
            List<TableAttr> tableAttrs = new ArrayList<TableAttr>();
            tableAttrs.add(new TableAttr(entity.getKeyField(), true, "TEXT"));
            tableAttrs.add(new TableAttr(displayField, false, "TEXT"));

            EntityDataDao entityDataDao = new EntityDataDao(context);
            entityDataDao.createTable(entity.getTableName(), tableAttrs);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public EntityRestClient getEntityRestClient() {
        String username = sharedPreferences.getString(PreferencesActivity.KEY_USERNAME,null);
        String password = sharedPreferences.getString(PreferencesActivity.KEY_PASSWORD,null);
        if (username.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            username = "super";
            password = "pass";
        }
        return ServiceGenerator.createService(EntityRestClient.class, username, password);
    }
}
