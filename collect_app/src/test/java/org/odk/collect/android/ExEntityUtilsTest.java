package org.odk.collect.android;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.odk.collect.android.external.model.ExEntity;
import org.odk.collect.android.utilities.ExEntityUtils;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Created by kakavi on 6/27/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21,packageName = "org.odk.collect.android")
public class ExEntityUtilsTest {

    public ExEntityUtilsTest() {

    }

    @Test
    public void testToTitleCase() {
        String toTitle = ExEntityUtils.toTitleCase("kakama victor");
        assertEquals("Kakama Victor", toTitle);
    }

    @Test
    public void testReconstructEntity() {
        String tableName = "table1";
        String displayField = "Name";
        String keyField = "id";

        ExEntity expected = new ExEntity(UUID.randomUUID().toString());
        expected.setTableName(tableName);
        expected.setDisplayField(displayField);
        expected.setKeyField(keyField);

        ExEntity actual = ExEntityUtils.reconstructEntity(tableName, displayField, keyField);
        assertEquals(expected.getTableName(), "table1");
        assertEquals(expected.getKeyField(), "id");
        assertEquals(expected.getDisplayField(), "Name");
    }

    @Test
    public void testReplaceLast() {
        assertEquals("abc", ExEntityUtils.replaceLast("abc", "d", "e"));
        assertEquals("abce", ExEntityUtils.replaceLast("abca", "a", "e"));
        assertEquals("ebc", ExEntityUtils.replaceLast("abc", "a", "e"));
        assertEquals("\"Position, fix, dial\\\"", ExEntityUtils.replaceLast("\"Position, fix, dial\"", "\"", "\\\""));
    }

    @Test
    public void testGenerateMisUrlFromKenga() throws Exception {
//        Uri mockUri = mock(Uri.class);
        String kengaUrl = "http://snvdata.org:8443/snvdata/mpsubmit/odk";
        String expected = "http://snvdata.org:8080/snv-mis/";
        assertEquals(expected, ExEntityUtils.generateMisUrlFromKenga(kengaUrl));
    }
}
